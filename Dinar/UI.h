/*
 * UI.h
 *
 * Created: 15.07.2015 9:19:12
 *  Author: tolyan
 */ 


#ifndef UI_H_
#define UI_H_

struct sDisplayCtrl;

struct UI_ctr {
	void (*Init)(void);
	void (*Draw)(struct sDisplayCtrl *display);
	void (*SwitchModeRequred)(void);
};

enum WorkMode {
	MODE_SIMPLE = 0,
	MODE_ADVANCED = 1,
	
	MODES_COUNT = 2
};

enum AdvModeEditingStep {
	EDIT_NONE = 0,
	EDIT_POS,
	EDIT_VALUE,
	EDIT_OVF,
};

void UI_setDisplay(struct sDisplayCtrl *d);
void updateDisplay(void);
void config_valve(void);
void UI_Init(void);
uint8_t modifyFloatTarget(float* val, float diff);
uint8_t modifyInt32(int32_t* val, int32_t diff);
uint8_t modifyUInt32(uint32_t* val, int32_t diff);
uint8_t modifyUInt16(uint16_t* val, int16_t diff);
char getStatusFlag(void);

#define UpArrow				0b11011001
#define DownArrow			0b11011010
#define Solid				0xff
#define degrie_c			0b11101111
#define kompressor_pic		0b11101101

extern const char Temperature[];
extern const char Ustavka[];
extern const char Disabled[];
extern const char degrie[];

#endif /* UI_H_ */