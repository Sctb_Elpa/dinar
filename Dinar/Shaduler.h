/*
 * Shaduler.h
 *
 * Created: 27.05.2014 14:08:22
 *  Author: tolyan
 */ 


#ifndef UPDATE_MANAGER_H_
#define UPDATE_MANAGER_H_

#include <stdint.h>

#define DOUBLE_REFRASH_RATE		1

typedef void (*taskRoutine)(void);

struct Task
{
	taskRoutine Routine;
	float period;	
};

void Shaduler_enable(uint8_t enable);
void Tick();
void ProcessShaduled();
uint8_t shadule(taskRoutine routine);
void shaduler_ProcessTimeCrytical();
void Shaduler_wait(uint32_t delayus);

#endif /* INCFILE1_H_ */