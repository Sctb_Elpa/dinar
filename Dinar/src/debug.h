/*
 * debug.h
 *
 * Created: 20.08.2014 11:34:05
 *  Author: tolyan
 */ 


#ifndef DEBUG_H_
#define DEBUG_H_

#include "config/AppConfig.h"

#include "ds1307.h"
#include <stdio.h>
#include <avr/pgmspace.h>

struct stackInfo
{
	uint8_t poscode;
	uint16_t _SP;
};

#define WATCH_STACK								0

#if WDT_STACK_USAGE == 1
#undef WATCH_STACK
#define WATCH_STACK								1
#endif

#define NVRAM_DEBUG_OFFSET						0

#define DEBUG_MSG(mft, arguments...)			printf_P(PSTR(mft), ## arguments)
#define DEBUG_STACK()							DEBUG_MSG("SP: %X", ((uint16_t)SPH << 8) + SPL)

#if WATCH_STACK
	#define DEBUG_SAVE_SP(posCode)	\
	do \
	{ \
		struct stackInfo stacknfo = {posCode, ((uint16_t)SPH << 8) + SPL}; \
		ds1307_writeNVRAM(NVRAM_DEBUG_OFFSET, &stacknfo, sizeof(struct stackInfo)); \
	} while (0)
#else
	#define DEBUG_SAVE_SP(posCode)	
#endif

#endif /* DEBUG_H_ */