#include "ds1307.h"
#include "regulator.h"

#include "dataOutputVars.h"


struct my_HID_Report dataOutputVars;


void updateTime(void)
{
	ds1307_getdate(&dataOutputVars.dateTime);
}

void updateCurrent()
{
	dataOutputVars.CurrentTemperature = getCurrentTemperature();
}

void updateTarget()
{
	dataOutputVars.TargetTemperature = getTargetTemperature();
}