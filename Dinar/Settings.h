/*
 * Settings.h
 *
 * Created: 29.05.2014 14:18:19
 *  Author: tolyan
 */ 


#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <avr/eeprom.h>

#include "debug.h"

#define DEFAULT_DisplayOffDelay			15

#define IDLE_TEMPERATURE				settings_getfloat(offsetof(struct sSettings, idletemperature))
#define SIMPLE_MODE_TARGET				settings_getfloat(offsetof(struct sSettings, SimpltModeTarget))

#define NVRAM_DISPLAYSTATUS_OFFSET		(NVRAM_DEBUG_OFFSET + sizeof(struct stackInfo))

#define SAVE_NVRAM(addr, val)			ds1307_writeNVRAM(addr, &val, 1);
#define READ_NVRAM(addr, pval)			ds1307_readNVRAM(&pval, addr, 1);

struct sSettings
{
	uint16_t uart_speed;
	uint8_t regulator_addr;
	uint16_t rx_timeout;
	float hysteresis;
	float idletemperature;
	float SimpltModeTarget;
	uint16_t stepmotor_move_speed;
	
	int32_t positions[2];
};

uint16_t settings_getu16(int addr);
uint8_t settings_getu8(int addr);
int32_t settings_gets32(int addr);
float settings_getfloat(int addr);
void settings_putfloat(int addr, float val);
void settings_putu16(int addr, uint16_t val);
void settings_puts32(int addr, int32_t val);

extern struct sSettings settings;

#endif /* SETTINGS_H_ */