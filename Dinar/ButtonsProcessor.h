/*
 * ButtonsProcessor.h
 *
 * Created: 30.05.2014 14:26:35
 *  Author: tolyan
 */ 


#ifndef BUTTONS_PROCESSOR_H_
#define BUTTONS_PROCESSOR_H_

#include <stdint.h>

enum enBtnID
{
	BTN_UP = 0,
	BTN_DOWN = 1,	
	BTN_SEL = 2,
	BTN_OK = 3,
	BTN_COUNT = 4,
	BTN_DUMMY = 0xff,
};

typedef void (*BtnCallBack)(void);

struct sButtonCallbacks
{
	uint8_t lastState;
	uint8_t LongPressDetected;
	BtnCallBack onRelease;
	BtnCallBack	onPress;
	BtnCallBack onLongPush;
	BtnCallBack onClick;
};

void RegularButtonsProcess();
void ButtonsInit();
void btns_clearCallbacks();
void btns_blockUnitlrelease();

extern struct sButtonCallbacks btns[BTN_COUNT];

#endif /* BUTTONS_PROCESSOR_H_ */