/*
 * UI.c
 *
 * Created: 15.07.2015 9:17:29
 *  Author: tolyan
 */ 

#include <math.h>

#include "regulator.h"
#include "Display.h"
#include "dataOutputVars.h"
#include "ButtonsProcessor.h"
#include "Coils.h"
#include "Termoprogramm.h"

#include "Lib/StepMotorController.h"

#include "UI.h"

const char Temperature[] = "T";
const char Ustavka[] = {0b10101001, 'c', 0b10111111, 0};
const char Disabled[] = {'B', 0b11000011, 0b10111010, 0b10111011, 0};	
const char degrie[] = {' ', degrie_c, 'C', 0};

extern struct UI_ctr SimpleUI;
extern struct UI_ctr AdvancedUI;
extern struct UI_ctr StepmotorConfigUI;

static struct UI_ctr *currentCtrl;
static struct UI_ctr *prevCtrl;

static struct sDisplayCtrl *display;

static BtnCallBack cb_bak;

uint8_t modifyFloatTarget(float* val, float diff)
{
	if (*val == IDLE_TEMPERATURE) {
		return 0;
	} else {
		*val += diff;
		return 1;
	}
}

uint8_t modifyInt32(int32_t* val, int32_t diff)
{
	*val += diff;
	return 1;
}

uint8_t modifyUInt32(uint32_t* val, int32_t diff)
{
	if (*val == 0 && diff < 0) {
		return 0;
	} else {
		*val += diff;
		return 1;
	}
}

uint8_t modifyUInt16(uint16_t* val, int16_t diff)
{
	if (*val == 0 && diff < 0) {
		return 0;
		} else {
		*val += diff;
		return 1;
	}
}

static void push_ui_mode(struct UI_ctr * newmode) {
	prevCtrl = currentCtrl;
	currentCtrl = newmode;
	UI_Init();
}

static void mode_pop() {
	currentCtrl = prevCtrl;
	UI_Init();
	btns_blockUnitlrelease();
}

static void stepmotor_do_job_reenable(void)
{
	btns[BTN_SEL].onLongPush = config_valve;
	btns[BTN_SEL].onRelease = cb_bak;
}

void config_valve(void)
{
	push_ui_mode(&StepmotorConfigUI);
}

void UI_Init(void)
{
	currentCtrl->Init();
}

static void switchToSimpleMode(void)
{
	currentCtrl = &SimpleUI;
	UI_Init();
}

static void switchToAdvansedMode(void)
{
	currentCtrl = &AdvancedUI;
	UI_Init();
}

void UI_setDisplay(struct sDisplayCtrl *d)
{
	display = d;
	
	SimpleUI.SwitchModeRequred = switchToAdvansedMode;
	AdvancedUI.SwitchModeRequred = switchToSimpleMode;
	StepmotorConfigUI.SwitchModeRequred = mode_pop;
	
	switchToSimpleMode();
}

void updateDisplay(void)
{
	currentCtrl->Draw(display);
}

char getStatusFlag(void)
{
	uint8_t ststusFlag = Solid;

	if (dataOutputVars.startStop)
	{
		if (dataOutputVars.dateTime.second % 2)
		{
			if (fabs(dataOutputVars.CurrentTemperature - dataOutputVars.TargetTemperature) <= STEP_SWITCH_DELTA_MAX)
				ststusFlag = ' ';
			else
			{
				if (dataOutputVars.CurrentTemperature > dataOutputVars.TargetTemperature)
					ststusFlag = CoilGetState(COIL_KOMPR) ? kompressor_pic : DownArrow;
				else
					ststusFlag = UpArrow;
			}
				
		}
		else
			ststusFlag = ' ';
	}
	
	return ststusFlag;
}