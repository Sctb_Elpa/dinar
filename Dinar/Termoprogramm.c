/*
 * Termoprogramm.c
 *
 * Created: 26.08.2015 14:18:20
 *  Author: tolyan
 */ 

#include <avr/eeprom.h>

#include <stdbool.h>
#include <math.h>

#include "dataOutputVars.h"
#include "regulator.h"
#include "Coils.h"
#include "Settings.h"
#include "UI.h"

#include "Termoprogramm.h"

#define COLD_MAX_TARGET			(30)	// ������������ ����������� ���� ����, ��� ������� ����� ���������� ����������� (ex. 85* -> 25*)
										// ��� ��������� ������� ���� ����������� ������ ���������� �� �����.
#define COLD_REQ_MAX			(20)	// ������������ �����������, ���������� ������� ������� ����������� ������������ (� ��� ��� ������ ����)

//#define DEBUG_T					(38)

static struct StepData Termoprogramm_saved[] EEMEM = {
	{	
#ifdef DEBUG_T
		.stepID = T_PRG_STEP_1,
		.TargetTemperature = DEBUG_T - 7,
#else
		.stepID = T_PRG_STEP_IDLE,
#endif
	},
	{
#ifdef DEBUG_T
		.stepID = T_PRG_STEP_1,
		.TargetTemperature = DEBUG_T - 7,
		.Time = 20,
#else
		.stepID = T_PRG_STEP_IDLE,
#endif
	},
	{
#ifdef DEBUG_T
		.stepID = T_PRG_STEP_1,
		.TargetTemperature = DEBUG_T,
#else
		.stepID = T_PRG_STEP_IDLE,
#endif
	},
	{
#ifdef DEBUG_T
		.stepID = T_PRG_STEP_1,
		.TargetTemperature = DEBUG_T,
		.Time = 20,
#else
		.stepID = T_PRG_STEP_IDLE,
#endif
	},
	{
		.stepID = T_PRG_STEP_IDLE,
	},
	{
		.stepID = T_PRG_STEP_IDLE,
	},
	{
		.stepID = T_PRG_STEP_IDLE,
	},
	{
		.stepID = T_PRG_STEP_IDLE,
	},
	{
		.stepID = T_PRG_STEP_IDLE,
	},
	{
		.stepID = T_PRG_STEP_IDLE,
	},
	{
		.stepID = T_PRG_STEP_IDLE,
	},
};

static uint8_t lastSec;

// ������� ��������������, ������������ �� ����������
static enum TermProgStep currentStep = T_PRG_STEP_IDLE;
static struct sTermoprogramm *currentTermoProgramm = NULL;

struct StepData Tp_getStep(enum TermProgStep step)
{
	struct StepData res = { .stepID = -1 };
	if (step > T_PRG_STEP_IDLE && step < T_PRG_STEPS)
		eeprom_read_block(&res, &Termoprogramm_saved[step - 1], sizeof(struct StepData));

	return res;
}

uint8_t Tp_setStep(enum TermProgStep step, struct StepData *stepData)
{
	if (step > T_PRG_STEP_IDLE && step < T_PRG_STEPS)
	{
		eeprom_write_block(stepData, &Termoprogramm_saved[step - 1], sizeof(struct StepData));
		return 1;
	} else {
		return 0;
	}
}


int8_t isCoolingRequred(float StartTemperature, float TargetTemperature)
{
	if (StartTemperature > TargetTemperature)
	{
		// ����������
		if (TargetTemperature > COLD_MAX_TARGET)
		{
			 // ����� ����������� ��� ������������	
			 return 0;
		}
		else if (TargetTemperature < COLD_REQ_MAX)
		{
			 // �������� �������� �����������
			 return 1;
		} 
		else
		{
			//COLD_REQ_MAX < TargetTemperature < COLD_MAX_TARGET
			if (StartTemperature - TargetTemperature > 10)
			{
				// �������, ����� ��������� �����������
				return -1;
			}
			else
			{
				// ����� ����������� ��� ������������
				return 0;
			}
		}
	} 
	else
	{
		//������
		if (TargetTemperature > COLD_REQ_MAX)
		{
			// ������ ����� �� ����� �����������, ��� ����������� �� �����
			return 0;
		}
		else
		{
			// ������ ����� �� �����������, ��� ��� ��������� ������������
			return 1;
		}
	}

	return 0;
}

enum enStepDoCode getActionCode(float prev, float target)
{
	if (fabs(prev - target) <= STEP_SWITCH_DELTA_MAX)
		return STEP_HOLD;
	else
		return prev > target ? STEP_TEMP_DOWN : STEP_TEMP_UP;
}

void constructTermoprogramm(struct sTermoprogramm* termoprog, float StartTemperature)
{
	float StartTemperature_b = StartTemperature;
	for (uint8_t i = 0; i < termoprog->len; ++i)
	{
		struct StepData *pCurrentStep = &termoprog->steps[i];
		if (pCurrentStep->stepID != i + 1)
		{
			if (pCurrentStep->stepID == T_PRG_STEP_IDLE)
			{
				// ����� ��������� (������ ���)
				
				// ����������� �� ���������� ���� ���� �� ���� ����������� ���������
				if (i > 0)
					termoprog->steps[i - 1].isStopCoolingAfterStep = true;
				break;
			}
			pCurrentStep->stepID = i + 1; // ������ ��������� �����, ���� - ������
		}
		
		switch (isCoolingRequred(StartTemperature, pCurrentStep->TargetTemperature))
		{
			case 0:
				pCurrentStep->isCoolingRequred = false;
				pCurrentStep->isStopCoolingAfterStep = false;
				break;
			case 1:
				pCurrentStep->isCoolingRequred = true;
				pCurrentStep->isStopCoolingAfterStep = false;
				break;
			case -1:
				pCurrentStep->isCoolingRequred = true;
				pCurrentStep->isStopCoolingAfterStep = true;
				break;
		}
		if (pCurrentStep->TargetTemperature != StartTemperature)
			pCurrentStep->Time = 0;
		else if (pCurrentStep->Time > 0 && pCurrentStep->Time < (1 * 60))
			pCurrentStep->Time = 1 * 60;
		
		StartTemperature = pCurrentStep->TargetTemperature;
	}
	
	for (uint8_t i = 1; i < termoprog->len; ++i)
	{
		struct StepData *pCurrentStep = &termoprog->steps[i];
		struct StepData *pPrevStep = &termoprog->steps[i - 1];
		
		if (pCurrentStep->stepID == T_PRG_STEP_IDLE)
			break;
		
		// ���� �� ���������� ���� ���� ���������, ��� ��������� ����������� �� ����� ����, � ������� ��� ������� ������������
		// �� �������� ���������� ������������ ������
		if (pCurrentStep->isCoolingRequred && pPrevStep->isStopCoolingAfterStep)
			pPrevStep->isStopCoolingAfterStep = false;
			
		pCurrentStep->StepDoCode = getActionCode(pPrevStep->TargetTemperature, pCurrentStep->TargetTemperature);
	}
	
	if (termoprog->steps[0].stepID != T_PRG_STEP_IDLE)
		termoprog->steps[0].StepDoCode = getActionCode(StartTemperature_b, termoprog->steps[0].TargetTemperature);
}

int8_t isCoolingRequredCS()
{
	return currentTermoProgramm->steps[currentStep - 1].isCoolingRequred;
}

void ExecTermoprogramm(struct sTermoprogramm* termoprog)
{
	currentTermoProgramm = termoprog;
	currentStep = T_PRG_STEP_IDLE;
	if (termoprog->len > 0)
		dataOutputVars.startStop = 1;
}

void BreakTermoprogramm()
{
	dataOutputVars.startStop = 0;
	
	Regulator_setIdle();
	CoilSetState(COIL_KOMPR, 0); // ����. �����������
	currentStep = T_PRG_STEP_IDLE; // reset termoprog
	
	UI_Init();
}

// ������� � ���������� ���� ���� ����� ���� (currentStep 1..)
static void NextTermoprogStep(void)
{
	if (currentTermoProgramm->len == currentStep || currentTermoProgramm->steps[currentStep].stepID == T_PRG_STEP_IDLE)
		BreakTermoprogramm();
	else
	{
		setTargetTemperature(currentTermoProgramm->steps[currentStep].TargetTemperature);
		dataOutputVars.TargetTemperature = currentTermoProgramm->steps[currentStep].TargetTemperature;
		++currentStep;
		if (currentTermoProgramm->callback_stepSwitched)
			currentTermoProgramm->callback_stepSwitched(currentStep);
	}
}

void TermoprogrammTick_1s()
{
	if (lastSec != dataOutputVars.dateTime.second)
	{
		if (dataOutputVars.startStop)
		{
			// ����
			struct StepData *pCurrentStep = &currentTermoProgramm->steps[currentStep - 1];
						
			if (currentStep == T_PRG_STEP_IDLE)
			{
				// start
				if (currentTermoProgramm->steps[currentStep].stepID == T_PRG_STEP_IDLE)
					BreakTermoprogramm(); // illegal state
				else
					NextTermoprogStep();
			}
			else
			{
				// executing
				if (pCurrentStep->Time > 0)
				{
					// ���������
					--pCurrentStep->Time;
				} else if (pCurrentStep->Time == 0)
				{
					// ������/����������
					if (!isnan(dataOutputVars.CurrentTemperature))
					{
						if (pCurrentStep->StepDoCode == STEP_TEMP_UP &&
							pCurrentStep->TargetTemperature - dataOutputVars.CurrentTemperature <= STEP_SWITCH_DELTA_MAX)
							NextTermoprogStep();
						if (pCurrentStep->StepDoCode == STEP_TEMP_DOWN &&
							dataOutputVars.CurrentTemperature - pCurrentStep->TargetTemperature <= STEP_SWITCH_DELTA_MAX)
							NextTermoprogStep();
					}
					
					if (pCurrentStep->StepDoCode == STEP_HOLD)
						NextTermoprogStep();
				}
				CoilSetState(COIL_KOMPR, pCurrentStep->isCoolingRequred);
			}
		}
		
		lastSec = dataOutputVars.dateTime.second;
	}
}