/*
 * Dataflash.h
 *
 * Created: 14.08.2014 15:05:09
 *  Author: tolyan
 */ 


#ifndef DATAFLASH_H_
#define DATAFLASH_H_

/** Constant indicating the total number of dataflash ICs mounted on the selected board. */
#define DATAFLASH_TOTALCHIPS                 1

/** EXT eeprom size */
#define DATAFLASH_SIZE						 ((uint32_t)64 * 1024)

/** EXT eeprom page size */
#define DATAFLASH_PAGE_SIZE					 (128)

/** Total number of pages inside the board's dataflash IC. */
#define DATAFLASH_PAGES                      (DATAFLASH_SIZE / DATAFLASH_PAGE_SIZE)

static inline void Dataflash_Init(void) {}


#endif /* DATAFLASH_H_ */