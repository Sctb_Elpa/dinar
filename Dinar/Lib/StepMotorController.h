/*
 * StepMotorController.h
 *
 * Created: 30.05.2019 11:34:10
 *  Author: tolyan
 */ 


#ifndef STEPMOTORCONTROLLER_H_
#define STEPMOTORCONTROLLER_H_

typedef int32_t motor_pos_t;
typedef void (*move_complead_cb)(void* context, motor_pos_t position);

// �������� �������� � eeprom
struct sEepromElement {
	uint8_t number; // ����� �� �������
	motor_pos_t position; // ������� ��������
};

// ����������� ���������
struct sStepMotorController {
	struct sEepromElement* eeprom_base; // ������� ������ ���������
	size_t storage_size; // ����� ��������� � ��. (�� ������)
	
	motor_pos_t current_position; // ������� ������� ��������
	struct sEepromElement* next_wp; // ������� ��� ��������� ������
	uint8_t next_number; // ����� ���������� ��������
	
	// �������� ��� ������ ������� �� ����������
	motor_pos_t destination;
	move_complead_cb ready_cb;
	void* ready_context;
};

/************************************************************************/
/* ������������� ��������� ��������� ������� ��������                   */
/* ctx - ��������� �� ��������� ���������							    */
/* eeprom_base - ��������� �� ������� ��������� � EEPROM				*/
/* storage_size - ���������� ��������� ��� ������� ��.					*/
/************************************************************************/
void StepMotorController_init(struct sStepMotorController* ctx, const struct sEepromElement* eeprom_base, uint8_t storage_size);

/************************************************************************/
/* ����������� ��������. �������� �������� ������� ���������� �			*/
/* ��������� �������													*/
/************************************************************************/
void StepMotorController_goto(struct sStepMotorController* ctx, const motor_pos_t destination, 
							  move_complead_cb ready_cb, void* ready_ctx);
						
/************************************************************************/
/* �������� ������� ������� ������                                      */
/************************************************************************/
motor_pos_t StepMotorController_currentPos(const struct sStepMotorController* ctx);

/************************************************************************/
/* �������� ����� ����������                                            */
/************************************************************************/
motor_pos_t StepMotorController_destination(const struct sStepMotorController* ctx);

#endif /* STEPMOTORCONTROLLER_H_ */