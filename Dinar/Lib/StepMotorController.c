/*
 * StepMotorController.c
 *
 * Created: 30.05.2019 11:50:32
 *  Author: tolyan
 */ 

#include <stdbool.h>

#include <avr/eeprom.h>
#include <avr/interrupt.h>

#include "StepMotorController.h"

#include "debug.h"
#include "Shaduler.h"

#define STEPGEN_RATE		1000 // 1k pulses/s.
#define STEPGEN_DEVIDER		(F_CPU / 64 / STEPGEN_RATE / 2)
// F_CPU
// / 64 - counter prescaler -> STEPGEN_RATE RANGE = [2..173913]
// STEPGEN_RATE - designed stepgen rate pre second
// / 2 - one interrupt -> half of step

#define STEP_PORT		PORTC
#define STEP_DIR		DDRC
#define STEP_IN			PINC
#define STEP_PIN		2

#define DIR_PORT		PORTC
#define DIR_DIR			DDRC
#define DIR_IN			PINC
#define DIR_PIN			1

#define STEP_INVERT		true
#define DIR_INVERT		false

static volatile struct sStepMotorController* current_working_ctx = NULL;

static void prepare_timer() {
	TCCR1A = 0;
	TCCR1B = 1 << WGM12;  // ������������� ����� ��� (����� �� ����������)
	TIMSK1 |= 1 << OCIE1A; // ������������� ��� ���������� ���������� 1��� �������� �� ���������� � OCR1A(H � L)
	
	// ����� ��� ���������
	OCR1AH = STEPGEN_DEVIDER >> 8;
	OCR1AL = STEPGEN_DEVIDER & 0xff;
	
	DEBUG_MSG("OCR1AH=0x%x\n", OCR1AH);
	DEBUG_MSG("OCR1AL=0x%x\n", OCR1AL);
}

static void start_timer() {
	TCCR1B |= (1 << CS11) | (1 << CS10); // prescaler = 64
}

static void stop_timer() {
	TCCR1B &= ~((1 << CS11) | (1 << CS10)); // timer stopped
}

static void findLastValidElement(struct sStepMotorController* ctx, struct sEepromElement** ppLastValidRecord) {
	struct sEepromElement elemet;
	eeprom_read_block(&elemet, ctx->eeprom_base, sizeof(struct sEepromElement));
	uint8_t this_element_number = elemet.number;
	
	uint8_t offset = 1;
	for (; offset < ctx->storage_size; ++offset)
	{
		eeprom_read_block(&elemet, &ctx->eeprom_base[offset], sizeof(struct sEepromElement));
		if (elemet.number == this_element_number + 1) {
			// normal
			this_element_number++;
			continue;
		} else {
			// sequence break
			break;
		}
	}
	// all sequence from first to last is valid, so restart sequence
	
	*ppLastValidRecord = &ctx->eeprom_base[offset - 1];
}

static struct sEepromElement* next_write_pos(const struct sStepMotorController* ctx, const struct sEepromElement* lastWritePos) {
	return (lastWritePos == &ctx->eeprom_base[ctx->storage_size - 1]) 
		? ctx->eeprom_base 
		: lastWritePos + 1;
}

static void step_next_wp(struct sStepMotorController* ctx) {
	ctx->next_number++;
	ctx->next_wp = next_write_pos(ctx, ctx->next_wp);
}

static void save_position(const struct sStepMotorController* ctx) {
	struct sEepromElement save_element = {ctx->next_number, ctx->current_position};
	eeprom_write_block(&save_element, ctx->next_wp, sizeof(struct sEepromElement));
}

static void dump_position_storage(const struct sStepMotorController *ctx) {
	DEBUG_MSG("Storage dump:\n");
	for (uint8_t i = 0; i < ctx->storage_size; ++i) {
		struct sEepromElement storage_element;
		eeprom_read_block(&storage_element, &ctx->eeprom_base[i], sizeof(struct sEepromElement));
		DEBUG_MSG("%d: %d	%d\n", i, storage_element.number, storage_element.position);
	}
	DEBUG_MSG("\n");
}

static void finish_moution() {
	save_position(current_working_ctx);
	step_next_wp(current_working_ctx);
	//DEBUG_MSG("finish_moution(): next {0x%x, %d}\n", current_working_ctx->next_wp, current_working_ctx->next_number);
	dump_position_storage(current_working_ctx);
	if (current_working_ctx->ready_cb) {
		current_working_ctx->ready_cb(current_working_ctx->ready_context, current_working_ctx->current_position);
	}
	current_working_ctx = NULL;
}

void StepMotorController_init(struct sStepMotorController* ctx, const struct sEepromElement* eeprom_base, uint8_t storage_size) {
	ctx->eeprom_base = eeprom_base;
	ctx->storage_size = storage_size;
	
	DEBUG_MSG("SMC_init(0x%x, 0x%x, %u)\n", ctx, eeprom_base, storage_size);
	
	struct sEepromElement* pLastValidRecord;
	findLastValidElement(ctx, &pLastValidRecord);
	DEBUG_MSG("findLastValidElement()=0x%x\n", pLastValidRecord);
	
	struct sEepromElement elemet;
	eeprom_read_block(&elemet, pLastValidRecord, sizeof(struct sEepromElement));
	ctx->current_position = elemet.position;
	ctx->next_number = elemet.number + 1;
	ctx->next_wp = next_write_pos(ctx, pLastValidRecord);
	
	DEBUG_MSG("ctx->current_position=%d\n", ctx->current_position);
	DEBUG_MSG("ctx->next_number=%u\n", ctx->next_number);
	DEBUG_MSG("ctx->next_wp=0x%x\n", ctx->next_wp);
	
	// init pins
	STEP_DIR |= 1 << STEP_PIN;
	DIR_DIR |=1 << DIR_PIN;
	
#if STEP_INVERT
	STEP_PORT |= 1 << STEP_PIN;
#else
	STEP_PORT &= ~(1 << STEP_PIN);
#endif
	DIR_PORT |= 1 << DIR_PIN;

	prepare_timer();
}

motor_pos_t StepMotorController_currentPos(const struct sStepMotorController* ctx) {
	return ctx->current_position;
}

motor_pos_t StepMotorController_destination(const struct sStepMotorController* ctx) {
	return ctx->destination;
}

void StepMotorController_goto(struct sStepMotorController* ctx, const motor_pos_t destination,
						      move_complead_cb ready_cb, void* ready_ctx) {
	if (current_working_ctx != NULL) {
		// current work in progress, return immediately
		if (ready_cb) {
			ready_cb(ready_ctx, ctx->current_position);
		}
		return;
	}
								  
	if (ctx->current_position != destination) {		  
		ctx->destination = destination;
		ctx->ready_cb = ready_cb;
		ctx->ready_context = ready_ctx;
		
		current_working_ctx = ctx;
		
		start_timer();
	} else {
		if (ready_cb) {
			ready_cb(ready_ctx, ctx->current_position);
		}
	}
}

void do_step() {
	bool direction = current_working_ctx->current_position < current_working_ctx->destination;
	
	if (current_working_ctx->current_position == current_working_ctx->destination) {
		stop_timer();
		shadule(finish_moution);
	} else {
		if (direction ^ DIR_INVERT) {
			DIR_PORT |= 1 << DIR_PIN;
		} else {
			DIR_PORT &= ~(1 << DIR_PIN);
		} 
		STEP_PORT ^= 1 << STEP_PIN;
		
		if (((STEP_PORT >> STEP_PIN) & 1) != STEP_INVERT) {
			if (direction) {
				++current_working_ctx->current_position;
			} else {
				--current_working_ctx->current_position;
			}
		}
	}
}

ISR (TIMER1_COMPA_vect)
{
	do_step();
}