/*
 * UI_Simple.c
 *
 * Created: 27.08.2015 9:19:07
 *  Author: tolyan
 */ 

#include <math.h>
#include <avr/pgmspace.h>

#include "Display.h"
#include "dataOutputVars.h"
#include "Settings.h"
#include "regulator.h"
#include "Termoprogramm.h"
#include "ButtonsProcessor.h"
#include "Coils.h"

#include "UI.h"

static uint8_t IsCooling;
static struct StepData simpleTermoprogramm_steps[2];
static struct sTermoprogramm simpleTermoprogramm = {
	.steps = simpleTermoprogramm_steps,
	.len = sizeof(simpleTermoprogramm_steps) / sizeof(struct StepData),
	.callback_stepSwitched = NULL
};

static void simple_mode_sel_callback(void);

static void Temperaturep1(void)
{
	if (!modifyFloatTarget(&dataOutputVars.TargetTemperature, 1.0))
		dataOutputVars.TargetTemperature = SIMPLE_MODE_TARGET;
}

static void Temperaturep10(void)
{
	if (!modifyFloatTarget(&dataOutputVars.TargetTemperature, 10.0))
		dataOutputVars.TargetTemperature = SIMPLE_MODE_TARGET;
}

static void Temperaturem1(void)
{
	if (!modifyFloatTarget(&dataOutputVars.TargetTemperature, -1.0))
		dataOutputVars.TargetTemperature = SIMPLE_MODE_TARGET;
}

static void Temperaturem10(void)
{
	if (!modifyFloatTarget(&dataOutputVars.TargetTemperature, -10.0))
		dataOutputVars.TargetTemperature = SIMPLE_MODE_TARGET;
}

static void simple_mode_ok_callback(void)
{
	if (dataOutputVars.startStop)
	{
		//perform stop
		BreakTermoprogramm();
		
		btns[BTN_UP].onClick = Temperaturep1;
		btns[BTN_DOWN].onClick = Temperaturem1;
		btns[BTN_UP].onLongPush = Temperaturep10;
		btns[BTN_DOWN].onLongPush = Temperaturem10;
		btns[BTN_SEL].onClick = simple_mode_sel_callback;
	}
	else
	{
		//perform start
		simpleTermoprogramm_steps[0].stepID = T_PRG_STEP_1;
		simpleTermoprogramm_steps[0].TargetTemperature = dataOutputVars.TargetTemperature;
		simpleTermoprogramm_steps[1].stepID = T_PRG_STEP_1;
		simpleTermoprogramm_steps[1].TargetTemperature = dataOutputVars.TargetTemperature;
		simpleTermoprogramm_steps[1].Time = -1; // infinity

		settings_putfloat(offsetof(struct sSettings, SimpltModeTarget), dataOutputVars.TargetTemperature);// save target
		constructTermoprogramm(&simpleTermoprogramm, dataOutputVars.CurrentTemperature);
		ExecTermoprogramm(&simpleTermoprogramm);
		
		btns[BTN_UP].onClick = NULL;
		btns[BTN_DOWN].onClick = NULL;
		btns[BTN_UP].onLongPush = NULL;
		btns[BTN_DOWN].onLongPush = NULL;
		btns[BTN_SEL].onClick = NULL;
	}
}

static void UI_init(void)
{
	setTargetTemperature(IDLE_TEMPERATURE);
	
	dataOutputVars.TargetTemperature = SIMPLE_MODE_TARGET;
	
	// config buttons
	btns_clearCallbacks();
	btns[BTN_UP].onClick = Temperaturep1;
	btns[BTN_DOWN].onClick = Temperaturem1;
	btns[BTN_UP].onLongPush = Temperaturep10;
	btns[BTN_DOWN].onLongPush = Temperaturem10;
	btns[BTN_SEL].onClick = simple_mode_sel_callback;
	btns[BTN_SEL].onLongPush = config_valve;
	btns[BTN_OK].onClick = simple_mode_ok_callback;
}


static void UI_update(struct sDisplayCtrl *display)
{
	Display_home(display);
	
	Display_SetCursor(display, 0, 0);
	Display_printf_P(display, PSTR("%3s = %.1f%s"), Temperature, (double)dataOutputVars.CurrentTemperature, degrie);
	for (uint8_t i = Display_currentX(display); i < 15; ++i)
		Display_putchar(display, ' ');
	Display_putchar(display, getStatusFlag());

	Display_SetCursor(display, 0, 1);
	Display_printf_P(display, PSTR("%3s = %.0f%s"), Ustavka, (double)dataOutputVars.TargetTemperature, degrie);
	Display_finishLine(display);
}


struct UI_ctr SimpleUI =
{
	.Draw = UI_update,
	.Init = UI_init,
};

static void simple_mode_sel_callback(void)
{
	if (SimpleUI.SwitchModeRequred)
		SimpleUI.SwitchModeRequred();
}