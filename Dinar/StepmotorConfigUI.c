/*
 * StepmotorConfigUI.c
 *
 * Created: 31.05.2019 13:37:22
 *  Author: tolyan
 */ 

#include <avr/pgmspace.h>

#include "Display.h"
#include "ButtonsProcessor.h"
#include "Settings.h"
#include "lib/StepMotorController.h"
#include "LUFA/LUFA/Common/common.h"

#include "UI.h"

#define MIN_STEP	8

enum enValveConfigState {
	MENU = 0,
	EDIT_SPEED = 1,
	MANUAL_MOVE = 2,
	POINT1 = 3,
	POINT2 = 4,
	
	enValveConfigState_COUNT = 5
};

struct sPointCtrl {
	uint8_t number;
	motor_pos_t position;
};

typedef void (*pfUpdateDisplay)(struct sDisplayCtrl *display);
typedef void (*pfMenucCtl)();

struct sSubmenu {
	pfMenucCtl init;
	pfUpdateDisplay update;
};

struct UI_ctr StepmotorConfigUI;

#if STEPMOTOR_ENABLED == 1
extern struct sStepMotorController stepmotor;
#endif

static const struct sSubmenu submenus[enValveConfigState_COUNT] PROGMEM;
static const char upr_ventelem[] PROGMEM = { ' ', ' ', 0xA9, 0xBE, 'p', '.', 'B', 'e', 0xBD, 0xBF, 'e', 0xBB, 'e', 0xbc, 0 };
	
static const char skor_vnet[] PROGMEM = { 'C', 0xBA, 'o', 'p', '.', 0xB3, 'e', 0xBD, 0xBF, 0xB8, 0xBB, '.', 0 };
static const char ruc4noy_rezhim[] PROGMEM = { 'P', 'y', 0xC0, 0xBD, 'o', 0xB9, ' ', 'P', 'e', 0xB6, 0xB8, 0xBC, 0 };
static const char tochka1[] PROGMEM = { 'T', 'o', 0xC0, 0xBA, 'a', ' ', '1', 0 };
static const char tochka2[] PROGMEM = { 'T', 'o', 0xC0, 0xBA, 'a', ' ', '2', 0  };
		
static const char *const menuitems[] PROGMEM = {
	skor_vnet, ruc4noy_rezhim, tochka1, tochka2
};
	
static enum enValveConfigState curent_state;
static uint8_t menuitem_n;
static motor_pos_t motor_destination;
static bool question_opened;

static struct sPointCtrl point_ctrl[2] = {
	{ 1, 0,	}, { 2, 0 }
};

static uint16_t move_speed;

static void Init_MOVE_MAKER();

static struct sSubmenu loadSubmenu() {
	struct sSubmenu submenu;
	memcpy_P(&submenu, &submenus[curent_state], sizeof(struct sSubmenu));
	return submenu;
}

static void Submenu_init() {
	loadSubmenu().init();
	question_opened = false;
}

static void save_and_exit() {	
	StepmotorConfigUI.SwitchModeRequred();
}

static void save_pos(uint8_t n, motor_pos_t val) {
	settings_puts32(offsetof(struct sSettings, positions) + n * sizeof(motor_pos_t), val);
}

static void menu_return() {
	btns_clearCallbacks();
	
	curent_state = MENU;
	Submenu_init();
}

static void UI_init() {
	menuitem_n = 0;
	menu_return();
}

static void UI_update(struct sDisplayCtrl *display) {
	Display_home(display);
	loadSubmenu().update(display);
}

/************************************************************************/

static void Draw_MENU(struct sDisplayCtrl *display){
	Display_SetCursor(display, 0, 0);
	Display_printf_P(display, upr_ventelem);
	Display_finishLine(display);
	
	Display_SetCursor(display, 0, 1);
	
	const char* pgmp_menuitem_str = pgm_read_ptr(&menuitems[menuitem_n]);
	size_t spaces = (display->_cols - 2 - strlen_P(pgmp_menuitem_str)) / 2;
	
	Display_putchar(display, '<');
	for (uint8_t i = 0; i < spaces; ++i) {
		Display_putchar(display, ' ');
	}
	Display_printf_P(display, pgmp_menuitem_str);
	Display_finishLine(display);
	Display_SetCursor(display, 15, 1);
	Display_putchar(display, '>');
} 

static void Draw_help(struct sDisplayCtrl *display) {
	static const char helpStr[] PROGMEM = {0xd9, 0xda, '-', 'B', 0xC3, 0xB2, '.', ' ', 'O', 'K', '-', 0x7E, ' ', 0xEF, '-', 0xD5, 0 };
	
	Display_SetCursor(display, 0, 1);
	Display_printf_P(display, helpStr);
}

static void Draw_EDIT_SPEED(struct sDisplayCtrl *display){
	static const char str1[] PROGMEM = {'C', 0xBA, 'o', 'p', 'o', 'c', 0xBF, 0xC4, 0x3A, ' ', '%', ' ', '6', 'u' , 0 };

	Display_SetCursor(display, 0, 0);
	Display_printf_P(display, str1, move_speed);
	
	Draw_help(display);
}

static void Draw_save_marker_dialog(struct sDisplayCtrl *display)  {
	static const char str1[] PROGMEM = {'C', 'o', 'x', 'p', '.', 0xBF, '.', ' ', '%', ' ', '8', 'd' , 0 };
	static const char str2[] PROGMEM = {0xDA, '-', 0xBF, '1', ' ', 0xD9, '-', 0xBF, '2', ' ', 0xEF, '-', 'O', 0xBF, 0xBC, '.' , 0 };
	
	Display_SetCursor(display, 0, 0);
	Display_printf_P(display, str1, StepMotorController_currentPos(&stepmotor));
	Display_SetCursor(display, 0, 1);
	Display_printf_P(display, str2);
	Display_finishLine(display);
}

static void Draw_MANUAL_MOVE(struct sDisplayCtrl *display){
	enum enMarker {
		MARKER_CURRENT = 1,
		MARKER_DEST = 3,
		MARKER_POS1 = 0,
		MARKER_POS2 = 2,
		
		MARKER_COUNT = 4,
	};
	
	struct sMarker {
		uint8_t marker;
		motor_pos_t pos;
		bool set;
	};
	
	// ��� � vfprintf() ������������ ��������� �������� ���� � ������ ��� ���� %d, ������� �� 2 ������
	static const char str1[] PROGMEM = { 0x40, '%', ' ', '6', 'd', ' ', '->', ' ', 0 };
		
	if (question_opened) {
		Draw_save_marker_dialog(display);
		return;
	}
		
	const motor_pos_t current_pos = StepMotorController_currentPos(&stepmotor);
	const motor_pos_t destination = motor_destination;
	const motor_pos_t pos1 = point_ctrl[0].position;
	const motor_pos_t pos2 = point_ctrl[1].position;
		
	Display_SetCursor(display, 0, 0);
	Display_printf_P(display, str1, current_pos);
	Display_printf_P(display, PSTR("% 6d"), motor_destination);
	
	// progress bar
	motor_pos_t scale_min = MIN(MIN(current_pos, destination), MIN(pos1, pos2));
	motor_pos_t scale_max = MAX(MAX(current_pos, destination), MAX(pos1, pos2));
	uint32_t diff_max = scale_max - scale_min;
		
	uint32_t scale = (diff_max / display->_cols) + 1;
	if (scale < MIN_STEP) {
		scale = MIN_STEP;
	}
	
	struct sMarker markers[] = { 
		{ '*', current_pos, false },
		{ 0xEE, destination, false },
		{ 0xDB, pos1, false },
		{ 0xDC, pos2, false }, 
	};
	
	// ������ ���� �������� ����� 1 � 2 �� ���� ���� ��� � �������� ������ ������
	uint8_t marker_count = MARKER_COUNT;
	if (abs(pos1 - pos2) < scale) {
		markers[2].marker = 0xDD;
		--marker_count;
	}
		
	Display_SetCursor(display, 0, 1);
	for (uint8_t element = 0; element < display->_cols; ++element) {
		motor_pos_t end_bloack = scale_min + scale * (element + 1);
		bool found  = false;
		
		// ����� ������� �������� - � ������� �������� ����������
		for (enum enMarker marker = MARKER_POS1; marker < marker_count; ++marker) {
			struct sMarker *pmarker = &markers[marker];
			if (!pmarker->set && (pmarker->pos <= end_bloack)) {
				if (!found) {
					found = true;
					Display_putchar(display, pmarker->marker);
				}
				pmarker->set = true;
			}
		}

		if (!found) {
			Display_putchar(display, '-');
		}
	}
}

static void Draw_POINT(struct sDisplayCtrl *display, struct sPointCtrl* pPointCtrl) {
	static const char str1[] PROGMEM = { 'T', 'o', 0xC0, 0xBA, 'a', ' ', '%', 'u', 0x3A, '%', ' ', '8', 'd' , 0 };

	Display_SetCursor(display, 0, 0);
	Display_printf_P(display, str1, pPointCtrl->number, pPointCtrl->position);
	
	Draw_help(display);
}

static void Draw_POINT1(struct sDisplayCtrl *display) { Draw_POINT(display, &point_ctrl[0]); }
static void Draw_POINT2(struct sDisplayCtrl *display) { Draw_POINT(display, &point_ctrl[1]); }
	
/************************************************************************/

static void Init_MENU() {
	void menu_next_element() {
		menuitem_n = (menuitem_n == (sizeof(menuitems) / sizeof(PGM_VOID_P)) - 1) ? 0 : menuitem_n + 1;
	}
	
	void menu_prev_element() {
		menuitem_n = menuitem_n == 0 ? (sizeof(menuitems) / sizeof(PGM_VOID_P)) - 1 : menuitem_n - 1;
	}
	
	void enter_submenu() {
		curent_state = menuitem_n + 1;
		Submenu_init();
	}
	
	btns[BTN_UP].onClick = menu_next_element;
	btns[BTN_DOWN].onClick = menu_prev_element;
	btns[BTN_OK].onClick = enter_submenu;
	btns[BTN_SEL].onLongPush = save_and_exit;
}

static void Init_EditSpeed() {
	btns_clearCallbacks();
	
	move_speed = settings_getu16(offsetof(struct sSettings, stepmotor_move_speed));
		
	void save_speed_and_return() {
		if (settings_getu16(offsetof(struct sSettings, stepmotor_move_speed)) != move_speed) {
			settings_putu16(offsetof(struct sSettings, stepmotor_move_speed), move_speed);
		}
		menu_return(); 
	}
	
	void speed_plus_1() {
		modifyUInt16(&move_speed, +1);
	}
	
	void speed_plus_10() {
		modifyUInt16(&move_speed, +10);
	}
	
	void speed_minus_1() {
		modifyUInt16(&move_speed, -1);
	}
	
	void speed_minus_10() {
		modifyUInt16(&move_speed, -10);
	}
	
	btns[BTN_OK].onClick = save_speed_and_return;
	btns[BTN_SEL].onClick = menu_return;
	
	btns[BTN_UP].onClick = speed_plus_1;
	btns[BTN_UP].onLongPush = speed_plus_10;
	btns[BTN_DOWN].onClick = speed_minus_1;
	btns[BTN_DOWN].onLongPush = speed_minus_10;
}

static void Init_MANUAL_MOVE() {
	void updateSavedPos(struct sPointCtrl *p) {
		p->position = settings_gets32(offsetof(struct sSettings, positions) + (p->number - 1) * sizeof(motor_pos_t));
	}
	
	void move_to() {
		StepMotorController_goto(&stepmotor, motor_destination, NULL, NULL);
	}
	
	void savePoint_request() {
		question_opened = true;
		Init_MOVE_MAKER();
	}
	
	void move_to_center() {
		motor_destination = (point_ctrl[0].position + point_ctrl[1].position) / 2;
		move_to();
	}
	
	void dest_plus_8() {
		modifyInt32(&motor_destination, +MIN_STEP);
	}
	
	void dest_plus_80() {
		modifyInt32(&motor_destination, MIN_STEP * 10);
	}
	
	void dest_minus_8() {
		modifyInt32(&motor_destination, -MIN_STEP);
	}
	
	void dest_minus_80() {
		modifyInt32(&motor_destination, -MIN_STEP * 10);
	}
	
	motor_destination = stepmotor.current_position;
	updateSavedPos(&point_ctrl[0]);
	updateSavedPos(&point_ctrl[1]);
		
	btns_clearCallbacks();
	btns[BTN_OK].onClick = move_to;
	btns[BTN_OK].onLongPush = savePoint_request;
	btns[BTN_SEL].onClick = menu_return;
	btns[BTN_SEL].onLongPush = move_to_center;
	
	btns[BTN_UP].onClick = dest_plus_8;
	btns[BTN_UP].onLongPush = dest_plus_80;
	btns[BTN_DOWN].onClick = dest_minus_8;
	btns[BTN_DOWN].onLongPush = dest_minus_80;
}

static void Init_POINT(struct sPointCtrl* pPointCtrl) {
	static struct sPointCtrl* currentPointCtrl = NULL;
						
	motor_pos_t getSavedPos(struct sPointCtrl* p) {
		return settings_gets32(offsetof(struct sSettings, positions) + (currentPointCtrl->number - 1) * sizeof(motor_pos_t));
	}
						
	void save_pos_and_return() {
		if (getSavedPos(currentPointCtrl) != currentPointCtrl->position) {
			save_pos(currentPointCtrl->number - 1, currentPointCtrl->position);
		}
		menu_return();
	}
	
	void pos_plus_1() {
		modifyInt32(&currentPointCtrl->position, +1);
	}
	
	void pos_plus_10() {
		modifyInt32(&currentPointCtrl->position, +10);
	}
	
	void pos_minus_1() {
		modifyInt32(&currentPointCtrl->position, -1);
	}
	
	void pos_minus_10() {
		modifyInt32(&currentPointCtrl->position, -10);
	}
	
	currentPointCtrl = pPointCtrl;
	currentPointCtrl->position = getSavedPos(currentPointCtrl);
	
	btns_clearCallbacks();
	btns[BTN_OK].onClick = save_pos_and_return;
	btns[BTN_SEL].onClick = menu_return;
	
	btns[BTN_UP].onClick = pos_plus_1;
	btns[BTN_UP].onLongPush = pos_plus_10;
	btns[BTN_DOWN].onClick = pos_minus_1;
	btns[BTN_DOWN].onLongPush = pos_minus_10;
}

static void Init_POINT1() { Init_POINT(&point_ctrl[0]); }
static void Init_POINT2() { Init_POINT(&point_ctrl[1]); }
	
static void Init_MOVE_MAKER() {
	void save_to_p1() {
		save_pos(0, stepmotor.current_position);
		Submenu_init();
	}
	
	void save_to_p2() {
		save_pos(1, stepmotor.current_position);
		Submenu_init();
	}
	
	btns_clearCallbacks();
	btns[BTN_SEL].onClick = Submenu_init;
	btns[BTN_UP].onClick = save_to_p1;
	btns[BTN_DOWN].onClick = save_to_p2;
}

static const struct sSubmenu submenus[enValveConfigState_COUNT] PROGMEM = {
	{ Init_MENU, Draw_MENU}, 
	{ Init_EditSpeed, Draw_EDIT_SPEED},
	{ Init_MANUAL_MOVE, Draw_MANUAL_MOVE},
	{ Init_POINT1, Draw_POINT1},
	{ Init_POINT2, Draw_POINT2}
};

/************************************************************************/

struct UI_ctr StepmotorConfigUI = {
	.Draw = UI_update,
	.Init = UI_init
};