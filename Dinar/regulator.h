/*
 * regulator.h
 *
 * Created: 04.12.2014 13:18:40
 *  Author: tolyan
 * Control regulator by mudbus
 */ 


#ifndef REGULATOR_H_
#define REGULATOR_H_

#include <stdint.h>
#include "Settings.h"

void regulator_init();
float getCurrentTemperature();
float getTargetTemperature();
uint8_t setTemperatureHisteresis(float histeresis);
uint8_t setTargetTemperature(float target);
uint8_t setTemperatureAlarm(float alarm);
uint8_t Regulator_setIdle();

#endif /* REGULATOR_H_ */