В винде 8.1+ загрузчик DFU некорректно работает. Проблема в BOS дескрипторе, который связан с USB3+
Винда получает некорректный ответ и поэтому устройство недоинициализируется.

Решение : https://youtu.be/_Utrb5hNRZk
Вкратце: Нужно создать в ветви реестра HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\usbflags\
Раздел с именем vvvvpppprrrr
Где vvvv - vid, pppp - pid, rrrr - revision
В котором создать ключ типа Binary SkipBOSDescriptorQuery со сзначением 01 00 00 00
После этого перевоткнуть устройство